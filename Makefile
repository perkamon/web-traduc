PO4AFLAGS = -k 100
VERSION = 3.64-1

LANGS = fr

all: html

setuphtml:
	-@rm -rf build
	@cp -a ../man-pages-fr/build .
	@cp -a html build/
	@for i in $$(seq 8); do mkdir -p build/html/online/man$$i; done

html: setuphtml
	@set -e; last=x; for man in build/C/man*/*; do \
	  secman=$${man#build/C/}; \
	  page=build/fr/$$secman; \
	  test -e $$page || continue; \
	  name=$$(echo $$secman | sed -e 's,^man./\(.*\)\.\([0-9]\)$$,\1(\2),'); \
	  section=$$(echo $$secman | sed -e 's,^man\(.\)/.*$$,\1,'); \
	  if test $$section != $$last; then \
	    test $$last = x || echo '</ul>' >> build/html/online/index.html; \
	    printf '<h2 id="section%s"><a name="section%s" href="#toc">Section %s</a></h2>\n' $$section $$section $$section >> build/html/online/index.html; \
	    echo '<ul class="list">' >> build/html/online/index.html; \
	    last=$$section; \
	  fi; \
	  man2html -r $$page |\
	    sed -e '/Content-type: text.html/d' \
	        -e 's,&lt;URL:\(<A HREF="[^"]*">[^<>]*</A>\)&gt;,\1,' \
	        -e 's,&lt;<A HREF="file://[^"]*">\([^<>]*\)</A>&gt;,\&lt;\1\&gt;,' \
	        -e '/^Section:/,/<HR>/{s/Section: //; s/Updated: //; s/<BR><A .*/<HR>/; s/<A .*//; }' \
	        -e 's,<HEAD>,<head><link rel="stylesheet" title="Default Style" type="text/css" href="../styles.css"><meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />,' \
	        -e 's,<BODY>,<body><div id="header"><div id="logo"><a href="http://traduc.org/"><img src="http://www.traduc.org/moin_static/traduc_grand_nessie.png" alt="Traduc.org" /></a> v$(VERSION)</div><div id="navbar"><a href="../">Index</a> <a href="http://traduc.org/perkamon/Contribuer">Signaler une erreur</a> <a href="http://traduc.org/perkamon/Télécharger">Télécharger les traductions</a> <a href="http://www.kernel.org/doc/man-pages/online/pages/'$$secman'.html">Page de manuel en anglais</a></div></div><div class="content">,' \
	        -e 's,</BODY>,</div></body>,' \
	      > build/html/online/$$secman.html; \
	  title=$$(lexgrog "$$page" | sed -e '1!d' -e 's/.*: \".* - //' -e 's/"$$//'); \
	  printf "<li><a href=\"%s\">%s</a> - %s</li>\n" $$secman.html "$$name" "$$title" >> build/html/online/index.html; \
	done
	@printf "</ul>\n</div>\n\n</body>\n</html>\n" >> build/html/online/index.html

upload:
	scp -r build/html/online traduc.org:/home/traduc.org/www/perkamon.traduc.org/
	ssh traduc.org chmod -R g+rw /home/traduc.org/www/perkamon.traduc.org/

.PHONY: setuphtml
